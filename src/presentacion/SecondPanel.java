package presentacion;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

import negocio.Producto;
import persistencia.DBProducto;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class SecondPanel extends JPanel {
	public static JTextField textField = new JTextField();
	JComboBox comboBox = new JComboBox();
	public static JTextField textField_1 = new JTextField();
	public static JTextField textField_2 = new JTextField();
	private JCheckBox checkBox = new JCheckBox("");
	private JCheckBox checkBox_1 = new JCheckBox("");
	private JCheckBox checkBox_2 = new JCheckBox("");
	public static JButton btnCancelar = new JButton("Cancelar");
	public static JButton btnNewButton = new JButton("Guardar");
	DBProducto p = new DBProducto();

	public SecondPanel() {
		setLayout(null);

		textField.setBounds(87, 143, 150, 20);
		add(textField);
		textField.setColumns(10);

		JLabel lblAgregarProducto = new JLabel("Agregar Producto");
		lblAgregarProducto.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAgregarProducto.setBounds(10, 11, 150, 34);
		add(lblAgregarProducto);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 56, 46, 14);
		add(lblNombre);

		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(10, 115, 71, 14);
		add(lblDescripcion);

		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(10, 146, 46, 14);
		add(lblPrecio);

		JLabel lblTac = new JLabel("T.A.C.");
		lblTac.setBounds(10, 179, 71, 14);
		add(lblTac);

		JLabel lblGas = new JLabel("GAS");
		lblGas.setBounds(10, 200, 71, 14);
		add(lblGas);

		JLabel lblAlcohol = new JLabel("Alcohol");
		lblAlcohol.setBounds(10, 221, 57, 14);
		add(lblAlcohol);

		checkBox.setBounds(87, 212, 97, 23);
		add(checkBox);

		checkBox_1.setBounds(87, 191, 97, 23);
		add(checkBox_1);

		checkBox_2.setBounds(87, 170, 37, 23);
		add(checkBox_2);

		textField_1.setColumns(10);
		textField_1.setBounds(87, 112, 150, 20);
		add(textField_1);

		textField_2.setColumns(10);
		textField_2.setBounds(87, 53, 150, 20);
		add(textField_2);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.insert(new Producto(textField_2.getText(), Long.valueOf(textField.getText()), textField_1.getText(),
						checkBox.isSelected(), checkBox_1.isSelected(), checkBox_2.isSelected(),comboBox.getSelectedItem().toString(),true));
				JOptionPane.showMessageDialog(null, "Producto Agregado");
			}
		});

		btnNewButton.setBounds(87, 242, 89, 23);
		add(btnNewButton);

		btnCancelar.setBounds(186, 242, 89, 23);
		add(btnCancelar);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(10, 84, 71, 14);
		add(lblTipo);
		
		DefaultComboBoxModel aModel = new DefaultComboBoxModel(new String[] {"Entrada", "Plato Principal", "Postre", "Acompa\u00F1amiento", "Bebida"});
		comboBox.setModel(aModel);
		comboBox.setBounds(87, 81, 150, 20);
		add(comboBox);

	}
}



