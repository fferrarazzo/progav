package negocio;

public class Producto {

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getPrecio() {
		return precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getTac() {
		return tac;
	}

	public void setTac(Boolean tac) {
		this.tac = tac;
	}

	public Boolean getGas() {
		return gas;
	}

	public void setGas(Boolean gas) {
		this.gas = gas;
	}

	public Boolean getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Producto(String nombre, Long precio, String descripcion, Boolean tac, Boolean gas, Boolean alcohol,
			String tipo, Boolean habilitado) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.descripcion = descripcion;
		this.tac = tac;
		this.gas = gas;
		this.alcohol = alcohol;
		this.setTipo(tipo);
		this.habilitado = habilitado;
	}

	private String nombre;
	private Long precio;
	private String descripcion;
	private Boolean tac;
	private Boolean gas;
	private Boolean alcohol;
	private String tipo;
	private Boolean habilitado;
}
