package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import negocio.Producto;

public class DBProducto {
	Conexion c = new Conexion();

	public boolean insert(Producto p) {
		try {
			PreparedStatement ps = c.Conexion().prepareStatement("INSERT INTO producto VALUES (?,?,?,?,?,?,?,?)");
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getDescripcion());
			ps.setLong(3, p.getPrecio());
			ps.setBoolean(4, p.getTac());
			ps.setBoolean(5, p.getGas());
			ps.setBoolean(6, p.getAlcohol());
			ps.setString(7, p.getTipo());
			ps.setBoolean(8, p.getHabilitado());
			return ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean actualizarProd(Producto p) {
		try {
			PreparedStatement ps = c.Conexion().prepareStatement(
					"UPDATE producto SET nombre = ?,descripcion = ?,precio = ?,tac = ?,gas = ?,alcohol = ?,tipo = ?,habilitado = ? WHERE producto.nombre = "
							+ p.getNombre());
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getDescripcion());
			ps.setLong(3, p.getPrecio());
			ps.setBoolean(4, p.getTac());
			ps.setBoolean(5, p.getGas());
			ps.setBoolean(6, p.getAlcohol());
			ps.setString(7, p.getTipo());
			ps.setBoolean(8, p.getHabilitado());
			return ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deshabilitar(Producto p) {
		try {
			PreparedStatement ps = c.Conexion().prepareStatement(
					"UPDATE producto SET habilitado = 0 WHERE producto.nombre = ? and producto.tipo = ?");
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getTipo());
			System.out.println(ps);
			return ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<Producto> obtenerProd(String desc) throws SQLException {
		List<Producto> listProd = new ArrayList<Producto>();
		try {
			Statement s = c.Conexion().createStatement();
			String q = "select * from producto where tipo = '" + desc + "' and habilitado = true";
			System.out.println(q);
			ResultSet rs = s.executeQuery(q);
			while (rs.next()) {
				listProd.add(new Producto(rs.getString("nombre"), rs.getLong("precio"), rs.getString("descripcion"),
						rs.getBoolean("tac"), rs.getBoolean("gas"), rs.getBoolean("alcohol"), rs.getString("tipo"),
						rs.getBoolean("habilitado")));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			c.Conexion().close();
		}
		return listProd;
	}

	public List<Producto> obtenerProd() throws SQLException {
		List<Producto> listProd = new ArrayList<Producto>();
		try {
			Statement s = c.Conexion().createStatement();
			String q = "select * from producto";
			System.out.println(q);
			ResultSet rs = s.executeQuery(q);
			while (rs.next()) {
				listProd.add(new Producto(rs.getString("nombre"), rs.getLong("precio"), rs.getString("descripcion"),
						rs.getBoolean("tac"), rs.getBoolean("gas"), rs.getBoolean("alcohol"), rs.getString("tipo"),
						rs.getBoolean("habilitado")));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			c.Conexion().close();
		}
		return listProd;
	}
}
