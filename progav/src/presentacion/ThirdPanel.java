package presentacion;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import persistencia.DBProducto;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import negocio.Producto;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ThirdPanel extends JPanel {
	private JTable table = new JTable();
	private JButton btnBorrar = new JButton("Borrar");
	private List<Producto> listProd = new ArrayList<Producto>();
	private String[] columnNames= new String[] { "Tipo","Producto","Descripcion", "Precio", "TAC","GAS","Alcohol","Habilitado" };
	private DBProducto p = new DBProducto();

	public ThirdPanel() {
		setLayout(null);
		try {
			listProd = p.obtenerProd();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DefaultTableModel dtm = new DefaultTableModel(getDatos(), columnNames) {
		};
		table.setModel(dtm);
		JScrollPane SP1 = new JScrollPane(table);
		SP1.setBounds(10, 11, 836, 389);
		this.add(SP1);
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() != -1){
					p.deshabilitar(listProd.get(table.getSelectedRow()));
					try {
						listProd = p.obtenerProd();
						DefaultTableModel dtm2 = new DefaultTableModel(getDatos(), columnNames) {
						};
						table.setModel(dtm2);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		
		btnBorrar.setBounds(10, 411, 89, 23);
		add(btnBorrar);
	}
	public Object[][] getDatos() {
		String[][] datos = new String[listProd.size()][columnNames.length];
		for (int i = 0; i < listProd.size(); i++) {
			int j = columnNames.length;
			datos[i][j - 8] = listProd.get(i).getTipo();
			datos[i][j - 7] = listProd.get(i).getNombre();
			datos[i][j - 6] = listProd.get(i).getDescripcion();
			datos[i][j - 5] = String.valueOf(listProd.get(i).getPrecio());
			datos[i][j - 4] = String.valueOf(listProd.get(i).getTac());
			datos[i][j - 3] = String.valueOf(listProd.get(i).getGas());
			datos[i][j - 2] = String.valueOf(listProd.get(i).getAlcohol());
			datos[i][j - 1] = String.valueOf(listProd.get(i).getHabilitado());
		}
		return datos;
	}
}
