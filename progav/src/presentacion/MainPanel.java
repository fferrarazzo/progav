package presentacion;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import negocio.Producto;
import persistencia.DBProducto;
import javax.swing.JButton;

public class MainPanel extends JPanel {
	private JTable table = new JTable();
	private JTable table_1 = new JTable();
	private JTable table_2 = new JTable();
	private JTable table_3 = new JTable();
	private JTable table_4 = new JTable();
	private List<Producto> listProd = new ArrayList<Producto>();
	private String[] columnNames= new String[] { "Producto", "Precio", "TAC" };
	private DBProducto p = new DBProducto();
	public JButton btnGenerarMenu = new JButton("Generar Menu");

	public MainPanel() {
		setLayout(null);
		
		JLabel lblMenu = new JLabel("MENU");
		lblMenu.setBounds(276, 11, 46, 14);
		add(lblMenu);
		
		JLabel lblEntrada = new JLabel("Entrada");
		lblEntrada.setBounds(10, 47, 46, 14);
		add(lblEntrada);
		
		JLabel lblPlatoPrincipal = new JLabel("Plato Principal");
		lblPlatoPrincipal.setBounds(10, 143, 148, 14);
		add(lblPlatoPrincipal);
		
		JLabel lblAcompaamiento = new JLabel("Acompa\u00F1amiento");
		lblAcompaamiento.setBounds(10, 209, 125, 77);
		add(lblAcompaamiento);
		
		JLabel lblPostre = new JLabel("Postre");
		lblPostre.setBounds(10, 277, 115, 77);
		add(lblPostre);
		
		JLabel lblBebida = new JLabel("Bebida");
		lblBebida.setBounds(10, 365, 115, 90);
		add(lblBebida);
		
		btnGenerarMenu.setBounds(665, 466, 125, 23);
		add(btnGenerarMenu);
		
		JScrollPane SP1 = new JScrollPane(table);
		SP1.setBounds(135, 47, 655, 61);
		add(SP1);
		
		JScrollPane SP2 = new JScrollPane(table_1);
		SP2.setBounds(135, 119, 655, 79);
		add(SP2);
		
		JScrollPane SP3 = new JScrollPane(table_2);
		SP3.setBounds(135, 209, 655, 88);
		add(SP3);
		
		JScrollPane SP4 = new JScrollPane(table_3);
		SP4.setBounds(135, 308, 655, 77);
		add(SP4);
		
		JScrollPane SP5 = new JScrollPane(table_4);
		SP5.setBounds(135, 394, 655, 61);
		add(SP5);
	}
	
	public void cargarTablas(){
		cargarTabla("Entrada",table);
		cargarTabla("Plato Principal",table_1);
		cargarTabla("Postre",table_2);
		cargarTabla("Acompaņamiento",table_3);
		cargarTabla("Bebida",table_4);
	}

	private void cargarTabla(String tipo, JTable table1) {
		try {
			listProd = p.obtenerProd(tipo);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DefaultTableModel dtm = new DefaultTableModel(getDatos(), columnNames) {
		};
		table1.setModel(dtm);
	}

	public Object[][] getDatos() {
		String[][] datos = new String[listProd.size()][columnNames.length];
		for (int i = 0; i < listProd.size(); i++) {
			int j = columnNames.length;
			datos[i][j - 3] = listProd.get(i).getNombre();
			datos[i][j - 2] = String.valueOf(listProd.get(i).getPrecio());
			datos[i][j - 1] = String.valueOf(listProd.get(i).getTac());
		}
		return datos;
	}
}
